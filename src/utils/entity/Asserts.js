/**
 * 参数断言工具类
 * Created by Conor Xie on 2018/4/1.
 */
import lodash from 'lodash'

export default class Asserts {
  /**
   * 注意 JS if 规则：
   * if([1, NaN, Infinity, true]) => true
   * if([0, null, undefined, false]) => false
   *
   * @param hasTrue {*} 为 true 时抛出异常消息
   * @param message {function(*): string} 异常消息
   */
  static hasTrue(hasTrue, message = () => '') {
    if (hasTrue) throw new Error(`参数校验失败:[js if 规则]:${message()}`)
  }

  /**
   * 注意 JS if 规则：
   * if([1, NaN, Infinity, true]) => true
   * if([0, null, undefined, false]) => false
   *
   * @param hasFalse {*} 为 false 时抛出异常消息
   * @param message {function(*): string} 异常消息
   */
  static hasFalse(hasFalse, message = () => '') {
    if (!hasFalse) throw new Error(`参数校验失败:[js if 规则]:${message()}`)
  }

  /**
   * 校验规则：null, undefined
   * @param value {*}
   * @param message {function(*): string} 异常消息
   */
  static hasNull(value, message = () => '') {
    if (lodash.hasNull(value)) throw new Error(`参数校验失败:[null,undefined]:${message()}`)
  }

  /**
   * 校验规则：null, undefined,'',[],
   * @param value {*}
   * @param message {function(*): string} 异常消息
   */
  static hasEmpty(value, message = () => '') {
    if (lodash.hasEmpty(value)) throw new Error(`参数校验失败:[非空]:${message()}`)
  }

  /**
   * 校验规则：!isEqual(left, right); 参考 locash 库：_.isEqual
   * @param left {*}
   * @param right {*}
   * @param message {function(*): string} 异常消息
   */
  static hasEquals(left, right, message = () => '') {
    if (!lodash.isEqual(left, right)) throw new Error(`参数校验失败:[相等]:${message()}`)
  }

  /**
   * 校验规则：!(min <= value && value <= max)
   * @param value {number}
   * @param min {number}
   * @param max {number}
   * @param message {function(*): string} 异常消息
   */
  static hasRange(value, [min, max], message = () => '') {
    if (!(min <= value && value <= max)) throw new Error(`参数校验失败:表达式不成立[${min} <= ${value} && ${value} <= ${max}]:${message()}`)
  }

  /**
   * 校验规则：!(min < value && value < max)
   * @param value {number}
   * @param min {number}
   * @param max {number}
   * @param message {function(*): string} 异常消息
   */
  static hasRound(value, [min, max], message = () => '') {
    if (!(min < value && value < max)) throw new Error(`参数校验失败:表达式不成立[${min} < ${value} && ${value} < ${max}]:${message()}`)
  }

  /**
   * 校验规则： !(value > 0)
   * @param value {number}
   * @param message {function(*): string} 异常消息
   */
  static gtZero(value, message = () => '') {
    if (!(value > 0)) throw new Error(`参数校验失败:表达式不成立[${value} > 0]:${message()}`)
  }

  /**
   * 校验规则：!(value >= 0)
   * @param value {number}
   * @param message {function(*): string} 异常消息
   */
  static geZero(value, message = () => '') {
    if (!(value >= 0)) throw new Error(`参数校验失败:表达式不成立[${value} >= 0]:${message()}`)
  }
}
