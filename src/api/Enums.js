/**
 * 枚举选项通用对象
 */
export class Item {
  /**
   *
   * @param key {string} 后台接口需要的值
   * @param comment {string} 前端显示值
   * @param deprecated {boolean} 是否废弃
   * @param checked {boolean} 默认是否选中
   */
  constructor(key, comment, {deprecated = false, checked = false} = {}) {
    /**
     * 后台接口需要的值
     * @type {string}
     */
    this.key = key
    /**
     * 前端显示值
     * @type {string}
     */
    this.comment = comment
    /**
     * 是否废弃
     * @type {boolean}
     */
    this.deprecated = deprecated
    /**
     * 默认是否选中
     * @type {boolean}
     */
    this.checked = checked
  }
}

/**
 * 角色权限代码
 */
export const AuthorityCode = {
  ROLE_ADMIN: new Item('ROLE_ADMIN', '管理员角色权限代码'),
  ROLE_USER: new Item('ROLE_USER', '普通用户'),
  home: new Item('home', '首页'),
  user: new Item('user', '用户管理'),
  user_list: new Item('user_list', '用户列表'),
  user_list_load: new Item('user_list_load', '查'),
  user_list_insert: new Item('user_list_insert', '增'),
  // user_list_delete: new Item('user_list_delete', '删'),
  user_list_update: new Item('user_list_update', '改'),
  user_list_copy: new Item('user_list_copy', '复制'),
  user_list_enable: new Item('user_list_enable', '启用'),
  user_list_disable: new Item('user_list_disable', '禁用'),
  user_list_reset_password: new Item('user_list_reset_password', '重置密码'),
  role_list: new Item('role_list', '角色列表'),
  role_list_load: new Item('role_list_load', '查'),
  role_list_insert: new Item('role_list_insert', '增'),
  role_list_delete: new Item('role_list_delete', '删'),
  role_list_update: new Item('role_list_update', '改'),
  role_list_copy: new Item('role_list_copy', '复制')
}

/**
 * 枚举状态：
 * @type {{SUCCESS: Item, RUNNING: Item, WATING: Item, options: (function(): Item[]), FAILURE: Item}}
 */
export const DemoStatus = {
  WATING: new Item('WATING', '等待中'),
  RUNNING: new Item('RUNNING', '执行中'),
  SUCCESS: new Item('SUCCESS', '成功'),
  FAILURE: new Item('FAILURE', '失败'),
  /**
   * 这里是为了 @type 自动生成选项
   * @return {Item[]}
   */
  options: () => Object.keys(DemoStatus)
    .filter(key => key !== 'options')
    .map(key => {
      const {comment, deprecated, checked} = DemoStatus[key]
      return new Item(key, comment, {deprecated, checked})
    })
}
