/**
 * 实体：用户
 * @author 谢长春 2019-7-28
 */

export default class User {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {User}
   * @return {User}
   */
  static of(obj) {
    return new User(obj || {})
  }

  /**
   * 构造参考案例参数：构造函数内部应该列出所有可能的参数，并对参数做说明
   * @param id {string} 数据 ID
   * @param dv {string} 数据版本
   * @param username {string} 登录名
   * @param password {string} 登录密码
   * @param nickname {string} 用户昵称
   * @param phone {string} 手机号
   * @param email {string} 邮箱
   * @param avatar {FileDTO} 用户头像
   * @param roles {string[]} 角色 ID 集合
   * @param domain {string} 该属性目前只有测试时使用，常规接口不用传值，测试接口传 test
   * @param createTime {string} 创建时间
   * @param createUserId {string} 创建用户ID
   * @param createUserNickname {string} 创建用户昵称
   * @param updateTime {string} 修改时间
   * @param updateUserId {string} 修改用户ID
   * @param updateUserNickname {string} 修改用户昵称
   * @param disabled {boolean} 是否禁用。 [0:否,1:是]
   * @param createTimeFormat {string} 创建时间格式化，仅用于前端展示
   * @param updateTimeFormat {string} 修改时间格式化，仅用于前端展示
   * @param deleted {boolean} 是否逻辑删除。 [0:否,1:是]
   * @param authorityList {string[]} 角色对应的权限指令集合
   * @param roleNames {String[]} 角色名称集合
   * @param roleId {string} 角色 id {@link Role#id}，按角色查询用户列表时使用该字段
   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   * @param containsAll {string} 模糊匹配：用户名/昵称/手机号/邮箱
   */
  constructor({
                id = undefined,
                dv = undefined,
                username = undefined,
                password = undefined,
                nickname = undefined,
                phone = undefined,
                email = undefined,
                avatar = undefined,
                roles = undefined,
                domain = undefined,
                createTime = undefined,
                createUserId = undefined,
                createUserNickname = undefined,
                updateTime = undefined,
                updateUserId = undefined,
                updateUserNickname = undefined,
                disabled = undefined,
                deleted = undefined,
                authorityList = undefined,
                roleNames = undefined,
                roleId = undefined,
                createTimeFormat = undefined,
                updateTimeFormat = undefined,
                orderBy = undefined,
                page = undefined,
                containsAll = undefined
              } = {}) {
    /**
     * 数据 ID
     * @type {number}
     */
    this.id = id
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv
    /**
     * 登录名
     * @type {string}
     */
    this.username = username
    /**
     * 登录密码
     * @type {string}
     */
    this.password = password
    /**
     * 用户昵称
     * @type {string}
     */
    this.nickname = nickname
    /**
     * 手机号
     * @type {string}
     */
    this.phone = phone
    /**
     * 邮箱
     * @type {string}
     */
    this.email = email
    /**
     * 用户头像
     * @type {FileDTO}
     */
    this.avatar = avatar
    /**
     * 角色 ID 集合
     * @type {string[]}
     */
    this.roles = roles
    /**
     * 该属性目前只有测试时使用，常规接口不用传值，测试接口传 test
     * @type {string}
     */
    this.domain = domain
    /**
     * 创建时间
     * @type {string}
     */
    this.createTime = createTime
    /**
     * 创建用户ID
     * @type {number}
     */
    this.createUserId = createUserId
    /**
     * 创建用户昵称
     * @type {string}
     */
    this.createUserNickname = createUserNickname
    /**
     * 修改时间
     * @type {string}
     */
    this.updateTime = updateTime
    /**
     * 修改用户ID
     * @type {number}
     */
    this.updateUserId = updateUserId
    /**
     * 修改用户昵称
     * @type {string}
     */
    this.updateUserNickname = updateUserNickname
    /**
     * 创建时间格式化，仅用于前端展示
     * @type {string}
     */
    this.createTimeFormat = createTimeFormat
    /**
     * 修改时间格式化，仅用于前端展示
     * @type {string}
     */
    this.updateTimeFormat = updateTimeFormat
    /**
     * 是否逻辑删除。 [0:否,1:是]
     * @type {boolean}
     */
    this.deleted = deleted
    /**
     * 是否禁用。 [0:否,1:是]
     * @type {boolean}
     */
    this.disabled = disabled
    /**
     * 角色对应的权限指令集合
     * @type {string[]}
     */
    this.authorityList = authorityList
    /**
     * 角色 id {@link Role#id}
     * @type {string}
     */
    this.roleId = roleId
    /**
     * 角色名称集合
     * @type {String[]}
     */
    this.roleNames = roleNames
    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
    /**
     * 模糊匹配：用户名/昵称/手机号/邮箱
     * @type  {string}
     */
    this.containsAll = containsAll
    /**
     * 当用户未上传头像时使用默认头像
     * @type {string}
     */
    this.avatarUrl = this.avatar?.url // || '/favicon.ico'
  }

  isDisabled() {
    return !!this.disabled
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象
   * @return {User}
   */
  clearBlankValue() {
    const obj = new User(JSON.parse(JSON.stringify(this)))
    if (obj.id === '') delete obj.id
    if (obj.createUserId === '') delete obj.createUserId
    if (obj.updateUserId === '') delete obj.updateUserId
    this.avatarUrl = undefined
    return obj
  }
}
