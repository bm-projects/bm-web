/**
 * 实体：文件对象
 * @author 谢长春 2020-03-10
 */
export default class FileDTO {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {FileDTO}
   * @return {FileDTO}
   */
  static of(obj) {
    return new FileDTO(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
   * @param name {string} 文件名，用户上传的文件名
   * @param uname {string} 唯一文件名，磁盘上存储的uuid文件名
   * @param url {string} 文件访问路径；原始图片访问路径
   * @param miniUrl {string} 等比缩放的缩略图，用于列表页面优化图片加载速度，点击该图片才显示原图
   * @param cropUrl {string} 裁剪过的图片，比如裁减方图用于9宫格，点击该图片才显示原图
   */
  constructor({
                name = undefined,
                uname = undefined,
                url = undefined,
                miniUrl = undefined,
                cropUrl = undefined
              } = {}) {
    /**
     * 文件名，用户上传的文件名
     * @type {string}
     */
    this.name = name
    /**
     * 唯一文件名，磁盘上存储的uuid文件名
     * @type {string}
     */
    this.uname = uname
    /**
     * 文件访问路径
     * 原始图片访问路径
     * @type {string}
     */
    this.url = url
    /**
     * 等比缩放的缩略图，用于列表页面优化图片加载速度，点击该图片才显示原图
     * @type {string}
     */
    this.miniUrl = miniUrl
    /**
     * 裁剪过的图片，比如裁减方图用于9宫格，点击该图片才显示原图
     * @type {string}
     */
    this.cropUrl = cropUrl
  }

  toString() {
    return JSON.stringify(this)
  }
}
