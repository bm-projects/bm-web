const plugins = []
const presets = []

if (process.env.NODE_ENV) { // vue 依赖编译插件
  presets.push('@vue/app')
} else { // gulpfile.babel.js 运行时依赖编译插件
  plugins.push('@babel/transform-runtime')
  presets.push('@babel/env')
}

module.exports = {plugins, presets}
