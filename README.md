# 说明文档

# 命令
```
# 启动开发环境，请求公网开发环境接口。 配置文件： .env.development
npm run dev

# 启动开发环境，请求本地 localhost:8080 服务接口。 配置文件： .env.local
npm run local

# dev 环境打包发布。 配置文件： .env.dev
npm run build:dev

# sit 环境打包发布。 配置文件： .env.sit
npm run build:sit

# uat 环境打包发布。 配置文件： .env.uat
npm run build:uat

# prod 环境打包发布。 配置文件： .env.production
npm run build
```

├── build  
│   └── index.js ..................... 打包模块报表    
├── doc .............................. 说明文档    
├── mock ............................. mock server    
├── public ........................... 静态资源     
├── src .............................. 源代码  
│   ├── api .......................... 后台接口服务  
│   ├── App.vue ...................... 应用主文件  
│   ├── assets ....................... 静态资源  
│   ├── components ................... 公共组件  
│   ├── icons ........................ 图标  
│   ├── layout ....................... 应用布局组件  
│   ├── main.js ...................... 应用主文件  
│   ├── permission.js ................ 权限  
│   ├── router ....................... 路由配置  
│   ├── settings.js  ................. 全局配置  
│   ├── store ........................ 数据缓存及状态管理  
│   ├── styles ....................... 自定义样式  
│   ├── utils ........................ 工具类  
│   └── views ........................ 页面视图组件  
├── .editorconfig ....................   
├── .env.development ................. 开发环境配置文件，文件配置的内容会放到 process.env 中   
├── .env.production .................. 生产环境配置文件，文件配置的内容会放到 process.env 中  
├── .env.staging ..................... 测试环境配置文件，文件配置的内容会放到 process.env 中，一般情况用不上这个环境  
├── .eslintignore .................... eslint 忽略文件  
├── .eslintrc.js ..................... eslint 规则配置文件  
├── .gitignore ....................... git 忽略文件  
├── gulpfile.babel.js ................ gulp 运行环境  
├── package.json ..................... 应用配置及依赖  
├── postcss.config.js ................ sass 配置  
└── vue.config.js .................... vue 配置  


### npm 依赖检查 
npm outdated  

### 解决 node-sass 安装慢
安装前先执行以下命令，加速安装 node-sass  
参考文档： https://github.com/lmk123/blog/issues/28  

> npm  

```
npm config set sass-binary-site http://npm.taobao.org/mirrors/node-sass
```

> yarn  

```
yarn config set sass-binary-site http://npm.taobao.org/mirrors/node-sass
```

### eslint 
使用 .eslintrc.js 规则覆盖 WebStorm 默认的代码格式化规则，避免格式化冲突  

![idea 设置](doc/imgs/eslint-1.png)
![应用 eslint 规则](doc/imgs/eslint-2.png)

### @ 路径前缀识别 
解决 WebStorm 无法识别 vue 组件带 @ 标记的路径前缀
![路径前缀识别](doc/imgs/vue-path.png)

### 前端性能优化
https://mp.weixin.qq.com/s/pC3xrjkyhTI7Ar-cTOph6A    
https://mp.weixin.qq.com/s/KThkpGTRLxPb0nyGkBylTQ    
https://zhuanlan.zhihu.com/p/74403911    

预生成 gzip
