import UserService from '@/api/UserService'
import User from '@/api/entity/User'
import {resetRouter} from '@/router'
import FileDTO from '@/api/entity/FileDTO'

const state = {
  userAvatar: new FileDTO(),
  userAvatarUrl: '',
  userId: '',
  userUuid: '',
  userName: '',
  userNickName: '',
  userPhone: '',
  userEmail: '',
  authorityList: []
}

const mutations = {
  RESET_STATE: (state) => {
    state.userAvatar = new FileDTO()
    state.userAvatarUrl = ''
    state.userId = ''
    state.userUuid = ''
    state.userName = ''
    state.userNickName = ''
    state.userPhone = ''
    state.userEmail = ''
    state.authorityList = state.authorityList.filter(() => false)
  },
  /**
   *
   * @param state
   * @param user {User}
   * @constructor
   */
  SET_USER: (state, user) => {
    state.userAvatar = user.avatar || new FileDTO()
    state.userAvatarUrl = user.avatar?.miniUrl || ''
    state.userId = user.id
    state.userUuid = user.uuid
    state.userName = user.username
    state.userNickName = user.nickname
    state.userPhone = user.phone
    state.userEmail = user.email
    state.authorityList = user.authorityList
  },
  /**
   *
   * @param state
   * @param avatar {FileDTO}
   * @constructor
   */
  SET_AVATAR: (state, avatar) => {
    state.userAvatar = avatar || new FileDTO()
    state.userAvatarUrl = avatar?.url || ''
  }
}

const actions = {
  // user login
  login({commit}, userInfo) {
    const {username, password} = userInfo
    return new Promise((resolve, reject) => {
      UserService.login(username, password)
        .then(result => {
          console.log(JSON.stringify(result))
          if (result.isSuccess()) {
            // result.dataFirst(
            //   data => {
            //     const vo = new User(data)
            //     commit('SET_USER', vo)
            //     setToken(vo.uuid)
            //   }
            // )
            resolve()
          } else {
            reject(result.message)
          }
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // get user info
  current({commit}, state) {
    return new Promise((resolve, reject) => {
      UserService.getCurrentUser()
        .then(result => {
          if (result.isSuccess()) {
            result.dataFirst(
              data => {
                const vo = new User(data)
                commit('SET_USER', vo)
                resolve(vo)
              },
              () => reject()
            )
          } else {
            reject(result.message)
          }
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // user logout
  logout({commit, dispatch}) {
    return new Promise((resolve, reject) => {
      UserService.logout().then(() => {
        resetRouter()
        dispatch('tagsView/delAllViews', null, {root: true})
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  clearToken({commit}) {
    return new Promise(resolve => {
      UserService.clearToken()
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

