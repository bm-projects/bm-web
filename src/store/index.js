import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// // https://webpack.js.org/guuides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', true, /\.js$/)

// you do not need `import app from './modules/app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

const store = new Vuex.Store({
  modules,
// : {
//     app,
//     errorLog,
//     settings,
//     user,
//     permission
//   },
  getters: {
    sidebar: state => state.app.sidebar,
    device: state => state.app.device,
    userAvatarUrl: state => state.user.userAvatarUrl,
    name: state => state.user.userNickName,
    authorityList: state => state.user.authorityList,
    routes: state => state.permission.routes,
    errorLogs: state => state.errorLog.logs,
    visitedViews: state => state.tagsView.visitedViews,
    cachedViews: state => state.tagsView.cachedViews
  }
})

export default store
