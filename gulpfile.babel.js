import gulp from 'gulp'
import lodash from 'lodash'
import env from 'gulp-env'
import fs from 'fs'
import path from 'path'
import readline from 'readline'
import './src/utils/string-prototype'
import './src/utils/date-prototype'
import xlsx from 'xlsx'
import Paths from './src/utils/entity/Paths'
import Aes from './src/utils/Aes'
import Rsa from "./src/utils/Rsa";

const options = require('minimist')(process.argv.slice(2), {})

env.set({
  ENV: 'development',
  NODE_ENV: 'development',
  VUE_APP_BASE_API: 'http://localhost:8080',
  VUE_APP_ENCRYPT_ENABLED: 'YES',
  GULPFILE: true,
  DEVELOPMENT: true,
  BROWSER: false,
  VUE_APP_AES_ENABLED: 'YES',
  VUE_APP_AES_SECRET_KEY: 'VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF',
  //VUE_APP_AES_IV: '2022010100112299'
})

gulp.task('default', async () => {
})

gulp.task('test', async () => {
  await import('./src/api/http')
  // await (await import('./test/api/CommonService.test')).default.csrfToken()

  // await (await import('./test/api/CodeAuthIdService.test.js')).default.testAll() // 测试代码模板接口
  // await (await import('./test/api/CodeOpenIdService.test.js')).default.testAll() // 测试代码模板接口
  // await (await import('./test/api/CodeSearchIdService.test.js')).default.testAll() // 测试代码模板接口
  // await (await import('./test/api/OpenCodeSearchIdService.test.js')).default.testAll() // 测试代码模板接口

  await (await import('./test/api/CodeExampleService.test')).default.testAll()
  await (await import('./test/api/RoleService.test')).default.testAll() // 测试角色接口
  await (await import('./test/api/UserService.test')).default.testAll() // 测试用户相关的接口
  await (await import('./test/api/UploadService.test')).default.testAll() // 测试通用模块接口
})
gulp.task('test:one', async () => {
  await import('./src/api/http')
  // await (await import('./test/api/UploadService.test')).default.csrfToken()
  await (await import('./test/api/UserService.test')).default.loginAdminBasic()

})

gulp.task('aes', async () => {
  // process.env.VUE_APP_AES_ENABLED='YES'
  // process.env.VUE_APP_AES_SECRET_KEY='VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF'
  // process.env.VUE_APP_AES_IV='2022010100112299'

  Aes.setEnabled('YES')
  Aes.setSecretKey('VnlSwQROeLeaZyXYqKFzdWmtsYseGwEF')

  console.log(Aes.decrypt('MDAwMTY0NTQ0NDU1NjAwNvvAt+nSiC/vRWuvBMFPcS6XeET6qKXOcBjfBIxamR+2Mbby087PzqTg8VwhRDCEd8FEWeRYVDy54E3PPSwHKrHEYR4uPVM9dDHoymqkFyxzl++FiSzEh7ygkJdtvOpm6N4MOa0XzwJ2LzBNKsseYoWTu2oi49klTrPv58hHQ3a9jZzgpCOF9/JzZWPnaY9EYHZnqW4L214vwn5bPly3m8ynzajiDamGeGyXr3ucQaL2rVad5rGQYro94rJDS6+vXk2E8VdPEQ3rtuBm+aMepGVKN8ak+QS5L8QZ5DVB+xZQShVHQfRQc8oRVrfAmohGl3EDN8zRP0WYi4vBIb8/SOYVQjmmiyEplxvuu8hlACJlkYP9asHMFVM91j/v1jFENynw2ojUcsuF49D6S6o9c+xn3NUy7MnTI0dNwb+ZPgPxGenAR40McOqsWLpWLKJUwb4na3dvgX/6Jj9/91T9QXv34VjUmFWOkIpDDO3QshMe2667ww3Oj4LIgiEc5YzgaU4AHnwqoAciOcShHp5/g2ucyd+YbLvght3auywjSBIitFFJl3OncWgLNZXFH0dLGVcJQdTg3U9pNO5MVq+jHnS3ccJOkKWW59enXTraSiCaUIkU31PpbFbqBq6G6L4BYNUZh55mjVx+eZpU4TMRz4PXc+21CmaTyedpo7IHfLp/vTN+lNC6Wqya/Wq4HBwJ07vWZZkObXUTEa6furpnq86t43x3MpzqOEOLHjMMc/M15WkZNUCu7MaMXpO0MBAjGrXE5/WnF+9NFgACKbkRPks939GUMH5zjD7JVLO/bqK2+OXMIAASnBRjo0NevwBlz+VJhGoFsKAJzhnN+xn8OAk='))
  console.log(Aes.decrypt('MDAwMTY0NTQ0NTEyMTcwN7BEKBitso1RUr8NkziaDKLxurXEgIwSiDnIFD3/UjuVC/kxnRwX+D+txsOQbiRXbMK8GcPRzwYxpO5qN0dK2ywzwJI3xA4LBBAGar/w6TehvMvTReMUrMmE1lgyJY/LGip4f2EFrLV6JFY/VGS475kFc0N+OW8Y8BIOdVONuX84vxiS7OCWKJo2eAz/aLp+7u/SIyai7YcQ4FW5889dEOr5qhkqqmk3iWA89Ef+4dYfutf8jUsUq7RQ0oktyfAet6NsDfWVxFM4YV82Y4loXHTowlkYB0jIEpgRBoORFQ5m8YCYBlXU6dg9X7VH2p9B21vAp8MUzv5K0WkD6RE8W7gg+ViEz5DynJDqbkjReMpJ'))

  console.log(JSON.stringify(['动态 IV 加密 Aes.encrypt', Aes.encrypt('测试 js 加密')]))
  console.log(JSON.stringify(['动态 IV 解密 Aes.decrypt', Aes.decrypt('MDAwMTY0NTQ0NDYyODY0OQOK1jx9D1vA+fy4VW7f2e8od4X3klGeQx67JB/uiIMC')]))
  console.log(JSON.stringify(['动态 IV 解密 java 加密数据', Aes.decrypt('MjAyMjAyMjEwMmxsdmNVV/Dmre0Gf0fTXDb9DCxOhWM=')]))

  Aes.setIV('2022010100112299')
  console.log(JSON.stringify(['静态 IV 加密 Aes.encrypt', Aes.encrypt('测试 js 加密')]))
  console.log(JSON.stringify(['静态 IV 解密 Aes.decrypt', Aes.decrypt('/IcOIjPiIvm3oKbNISzwdonBPfcHE4vxUiP22KTXdlU=')]))
  console.log(JSON.stringify(['静态 IV 解密 java 加密数据', Aes.decrypt('QR+MoBavrFsTrzFngtx6Vg==')]))
})

gulp.task('rsa', async () => {
  console.log(['公钥加密',Rsa.encrypt('123456')])
})

gulp.task('in:out', async () => {
  const writer = fs.createWriteStream(path.resolve('temp/out.txt'))
  writer.on('close', () => console.log('end'))
  readline.createInterface(
    fs.createReadStream(path.resolve('temp/in.txt')),
    writer
  )
    .on('line', line => {
      writer.write(line.concat(',\n'))
    })
    .on('close', () => writer.end())
})
gulp.task('write:line', async () => {
  const writer = fs.createWriteStream(path.resolve('temp/test.log'))
  writer.on('close', () => console.log('end'))
  writer.write('测试\n')
  writer.write('测试\n')
  writer.write('测试\n')
  writer.end()
})
gulp.task('read:line', async () => {
  readline.createInterface(fs.createReadStream(path.resolve('temp/test.log')))
    .on('line', text => {
      console.log(text)
    })
    .on('close', () => console.log('end'))
})

gulp.task('xlsx:read', async () => {
  const wb = xlsx.readFile(
    './temp/数据文件.xls'
  )
  const file = 'temp/数据文件.sql'
  const writer = fs.createWriteStream(file)
  writer.on('close', () => console.log(path.resolve(file)))
  // const sheet = wb.Sheets[wb.SheetNames.shift()];
  const sheet = wb.Sheets['SQL Results']
  sheet['!ref'] = sheet['!ref'].replace(/\w+:(\w+)/, 'B1:$1')
  const arrs = xlsx.utils.sheet_to_json(sheet)
    .map(row => {
      _.forEach(row, (v, k) => {
        if (v === '') delete row[k]
      })
      writer.write(
        `${Object.values(row).join(',')}\n`
      )
      return row
    })

  writer.end()
})
gulp.task('xlsx:write', async () => {
  const {data: {errorInfos: array}} = JSON.parse('')
  array.sort((a, b) => a.rowNumber - b.rowNumber)
  const wb = xlsx.utils.book_new()
  xlsx.utils.book_append_sheet(wb, xlsx.utils.json_to_sheet(array), 'Sheet1')
  xlsx.writeFile(wb, './temp/write.xlsx')
})

gulp.task('exec', async () => {
  const proc = require('child_process')
  proc.exec('ls', function (error, stdout, stderr) {
    console.log(stdout)
    if (error !== null) {
      console.log('exec error: ' + error)
    }
  })
})

gulp.task('stdout', async () => {
  process.stdin.setEncoding('utf8')
  process.stdin.on('readable', () => {
    const chunk = process.stdin.read()
    if (chunk) {
      process.stdout.write(`data: ${chunk}`)
      process.exitCode = 1
    }
  })
  process.stdin.on('end', () => {
    process.stdout.write('end')
  })
})
