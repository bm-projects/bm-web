import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/layout'
import {AuthorityCode} from '@/api/Enums'

Vue.use(Router)

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true,
    meta: {noCache: true}
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true,
    meta: {noCache: true}
  },
  {
    path: '/mdeditor',
    component: () => import('@/components/MdEditorDialog/demo'),
    hidden: true,
    meta: {noCache: true}
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard', // name 一定要跟组件名一样，默认的组件名是大驼峰命名
      component: () => import('@/views/dashboard'),
      meta: {title: '首页', icon: 'dashboard', affix: true}
    }]
  }
]
export const asyncRoutes = [
  {
    path: '/user',
    name: 'user',
    component: Layout,
    alwaysShow: true,
    redirect: '/user/list',
    meta: {title: '用户管理', icon: 'peoples', activeMenu: '/user/list', roles: [AuthorityCode.user.key]},
    children: [
      {
        path: 'list',
        name: 'User', // name 一定要跟组件名一样，默认的组件名是大驼峰命名
        component: () => import('@/views/user'),
        meta: {title: '用户列表', icon: 'user', roles: [AuthorityCode.user_list.key]}
      },
      {
        path: 'role',
        name: 'Role', // name 一定要跟组件名一样，默认的组件名是大驼峰命名
        component: () => import('@/views/user/role'),
        meta: {title: '角色列表', icon: 'tree', roles: [AuthorityCode.role_list.key]}
      }
    ]
  },
  {
    path: '/setting',
    name: 'setting',
    component: Layout,
    alwaysShow: true,
    redirect: 'noRedirect',
    meta: {title: '系统管理', icon: 'example', activeMenu: '', roles: [AuthorityCode.ROLE_ADMIN.key]},
    children: [
      /* {
        path: 'cache',
        name: 'Cache', // name 一定要跟组件名一样，默认的组件名是大驼峰命名
        component: () => import('@/views/setting/cache'),
        meta: {title: '缓存管理', icon: 'nested', roles: [AuthorityCode.ROLE_ADMIN.key]}
      },*/
      {
        path: 'error-log',
        name: 'ErrorCache', // name 一定要跟组件名一样，默认的组件名是大驼峰命名
        component: () => import('@/views/setting/error-log'),
        meta: {title: '异常日志', icon: 'nested', roles: [AuthorityCode.ROLE_ADMIN.key]}
      }
    ]
  },
  // {
  //   path: '/setting',
  //   name: 'setting',
  //   component: Layout,
  //   redirect: '/setting/personal',
  //   meta: {title: '设置', icon: 'example', activeMenu: '/setting/personal', roles: [AuthorityCode.Menu_Setting.key]},
  //   children: [
  //     {
  //       path: 'personal',
  //       name: 'personal',
  //       component: () => import('@/views/tree'),
  //       meta: {title: '个人设置', icon: 'table', roles: [AuthorityCode.Menu_Setting_Personal.key]}
  //     },
  //     {
  //       path: 'system',
  //       name: 'system',
  //       component: () => import('@/views/tree'),
  //       meta: {title: '系统设置', icon: 'table', roles: [AuthorityCode.Menu_Setting_System.key]}
  //     }
  //   ]
  // },
  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1'), // Parent router-view
  //       name: 'Menu1',
  //       meta: {title: 'Menu1'},
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1'),
  //           name: 'Menu1-1',
  //           meta: {title: 'Menu1-1'}
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2'),
  //           name: 'Menu1-2',
  //           meta: {title: 'Menu1-2'},
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
  //               name: 'Menu1-2-1',
  //               meta: {title: 'Menu1-2-1'}
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
  //               name: 'Menu1-2-2',
  //               meta: {title: 'Menu1-2-2'}
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3'),
  //           name: 'Menu1-3',
  //           meta: {title: 'Menu1-3'}
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2'),
  //       meta: {title: 'menu2'}
  //     }
  //   ]
  // },

  // 这里有一个需要非常注意的地方就是 404 页面一定要最后加载，如果放在constantRouterMap一同声明了404，后面的所以页面都会被拦截到404
  {path: '*', redirect: '/404', hidden: true}
]

const createRouter = () => new Router({
  base: `/${process.env.VUE_APP_PREFIX}`,
  mode: 'history', // 使用 history 模式时 nginx 需要配置   try_files $uri $uri/ /index.html;
  scrollBehavior: () => ({y: 0}),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
