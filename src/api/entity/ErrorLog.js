import lodash from 'lodash'

/**
 * 实体：异常日志记录
 * @author 谢长春 on 2022-02-22 V20210612
 */
export default class ErrorLog {
  /**
   * js 中， 类对象在经过方法传递后无法推断类型，造成类方法和变量提示不准确，这里 obj 转换之后可以得到正确的提示
   * @param obj {ErrorLog}
   * @return {ErrorLog}
   */
  static of(obj) {
    return new ErrorLog(obj)
  }

  /**
   * 构造函数：内部应该列出所有可能的参数，并对参数做说明
   * @param id {string} 数据ID
   * @param traceId {string} 链路追踪id
   * @param message {string} 日志内容
   * @param createTime {string} 错误发生时间，格式：yyyyMMddHHmmss
   * @param createTimeFormat {string} 错误发生时间
   * @param createUserId {string} 创建用户ID
   * @param updateTime {string} 处理时间，yyyyMMddHHmmssSSS
   * @param updateTimeFormat {string} 处理时间
   * @param updateUserId {string} 修改用户ID
   * @param deleted {boolean} 是否逻辑删除。 [0:否,1:是]
   * @param dv {string} 数据版本
   * @param createUserNickname {string} 创建用户昵称
   * @param createTimeFormat {string} 创建时间格式化，仅用于前端展示
   * @param updateUserNickname {string} 修改用户昵称
   * @param updateTimeFormat {string} 修改时间格式化，仅用于前端展示
   * @param orderBy {OrderBy[]} 排序字段集合
   * @param page {Page} 分页对象
   */
  constructor({
                id = undefined,
                traceId = undefined,
                message = undefined,
                createTime = undefined,
                createTimeFormat = undefined,
                createUserId = undefined,
                updateTimeFormat = undefined,
                updateTime = undefined,
                updateUserId = undefined,
                deleted = undefined,
                dv = undefined,
                createUserNickname = undefined,
                updateUserNickname = undefined,
                orderBy = undefined,
                page = undefined
              } = {}) {
    /**
     * 数据ID，主键自增
     * @type {string}
     */
    this.id = id
    /**
     * 链路追踪id
     * @type {string}
     */
    this.traceId = traceId
    /**
     * 日志内容
     * @type {string}
     */
    this.message = message
    /**
     * 错误发生时间，格式：yyyyMMddHHmmss
     * @type {string}
     */
    this.createTime = createTime
    /**
     * 错误发生时间，格式：yyyyMMddHHmmss
     * @type {string}
     */
    this.createTimeFormat = createTimeFormat
    /**
     * 创建用户ID
     * @type {number}
     */
    this.createUserId = createUserId
    /**
     * 处理时间，yyyyMMddHHmmssSSS
     * @type {string}
     */
    this.updateTime = updateTime
    /**
     * 处理时间，yyyyMMddHHmmssSSS
     * @type {string}
     */
    this.updateTimeFormat = updateTimeFormat
    /**
     * 修改用户ID
     * @type {number}
     */
    this.updateUserId = updateUserId
    /**
     * 是否逻辑删除。 [0:否,1:是]
     * @type {boolean}
     */
    this.deleted = deleted
    /**
     * 加密id
     * @type {string}
     */
    this.dv = dv
    /**
     * 创建用户昵称
     * @type {string}
     */
    this.createUserNickname = createUserNickname
    /**
     * 修改用户昵称
     * @type {string}
     */
    this.updateUserNickname = updateUserNickname
    /**
     * 排序字段集合
     * @type {OrderBy[]}
     */
    this.orderBy = orderBy
    /**
     * 分页对象
     * @type {Page}
     */
    this.page = page
  }

  toString() {
    return JSON.stringify(this)
  }

  /**
   * 清除空字符串字段，返回新对象。 前端组件输入框默认值为 "" 空字符串的情况，需要把数据类型不匹配的属性重置为 null
   * @return {ErrorLog}
   */
  clearBlankValue() {
    const obj = new ErrorLog(JSON.parse(JSON.stringify(this)))
    if (obj.id === '') delete obj.id
    if (obj.createUserId === '') delete obj.createUserId
    if (obj.updateUserId === '') delete obj.updateUserId
    if (obj.deleted === '') delete obj.deleted
    return obj
  }

  /**
   * 截断消息
   * @return {string}
   */
  truncateMessage() {
    return lodash.truncate(this.message || '', {length: 100})
  }
}
