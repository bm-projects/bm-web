import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import ErrorLog from './entity/ErrorLog'
import uuid from '@/utils/libs/uuid'

const pageURL = '/error-log/page/{number}/{limit}' // 分页：多条件批量查询
// const listURL = '/error-log' // 多条件批量查询，不分页
const findByIdURL = '/error-log/{id}' // 按 id 查询单条记录
const saveURL = '/open/error-log/{traceId}' // 新增
const updateURL = '/open/error-log/{dv}' // 更新
const markDeleteByIdURL = '/error-log/{dv}' // 按 dv 逻辑删除
const markDeleteByIdsURL = '/error-log' // 按 dv 批量逻辑删除

/**
 * 后台服务请求：异常日志记录
 * @author 谢长春 on 2022-02-22 V20220709
 */
export default class ErrorLogService {
  /**
   * 分页：多条件批量查询
   * @param vo {ErrorLog}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new ErrorLog()) {
    const {
      page,
      ...params
    } = vo
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new ErrorLog(row))
  }

//  /**
//   * 多条件批量查询，不分页
//   * @param vo {ErrorLog}
//   * @return {Promise<Result>}
//   */
//  static async list(vo = new ErrorLog()) {
//    /**
//     * @type {Result<any>}
//     */
//    const result = await http
//      .get(listURL, {params: vo})
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//    // 将响应结果转换为 js 类对象
//    return result.dataTransform(row => new ErrorLog(row))
//  }
//

  /**
   * 按 id 查询单条记录
   * @param id {string} {@link ErrorLog#id}
   * @return {Promise<Result>}
   */
  static async findById(id) {
    /**
     * @type {Result<any>}
     */
    const result = await http
      .get(findByIdURL.format(id || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    // 将响应结果转换为 js 类对象
    return result.dataTransform(row => new ErrorLog(row))
  }

  /**
   * 新增
   * @param traceId {string} {@link ErrorLog#traceId}
   * @param message {object}
   * @return {Promise<Result>}
   */
  static async save(traceId, message) {
    return await http
      .post(saveURL.format(traceId || uuid()), message)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 修改
   * @param vo {ErrorLog}
   * @return {Promise<Result>}
   */
  static async update(vo = new ErrorLog()) {
    const {id, dv, ...body} = vo
    return await http
      .put(updateURL.format(dv || 0), body)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv 逻辑删除
   * @param dv {string} {@link ErrorLog#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteById(dv) {
    return await http
      .patch(markDeleteByIdURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv 批量逻辑删除
   * @param dvs {string[]} {@link ErrorLog#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteByIds(dvs) {
    return await http
      .patch(markDeleteByIdsURL, dvs)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

//  /**
//   * 按 dv 删除
//   * @param dv {string} {@link ErrorLog#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteById(dv) {
//    return await http
//      .delete(deleteByIdURL.format(id || 0))
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//  /**
//   * 按 dv 批量删除
//   * @param dv {string[]} {@link ErrorLog#dv}
//   * @return {Promise<Result>}
//   */
//  static async deleteByIds(dvs) {
//    return await http
//      .delete(deleteByIdsURL, dvs)
//      .then(Result.ofResponse)
//      .catch(Result.ofCatch)
//  }
//
}
