import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import ElementUI from 'element-ui'
ElementUI.Dialog.props.closeOnClickModal.default = false
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss' // global css
import '@/utils/string-prototype' // String 原型扩展
import '@/utils/date-prototype' // Date 原型扩展
import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control
import '@/api/http' // permission control
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import echarts from 'echarts'

Vue.prototype.$echarts = echarts
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const {mockXHR} = require('../mock');
//   mockXHR();
// }

// set ElementUI lang to EN
// Vue.use(ElementUI, {locale})
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

Vue.config.productionTip = false
if (process.env.VUE_APP_MOCK_ENABLED === 'YES') {
  const {mockXHR} = require('../mock')
  mockXHR()
}
if (process.env.NODE_ENV === 'production') { // 记录异常日志
  Vue.config.errorHandler = function(err, vm, info, a) {
    // Don't ask me why I use Vue.nextTick, it just a hack.
    // detail see https://forum.vuejs.org/t/dispatch-in-vue-config-errorhandler-has-some-problem/23500
    Vue.nextTick(() => {
      store.dispatch('errorLog/addErrorLog', {
        err,
        vm,
        info,
        url: window.location.href
      })
      console.error(err, info)
    })
  }
}

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
