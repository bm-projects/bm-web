import http from './http'
import Page from '../utils/entity/Page'
import Result from '../utils/entity/Result'
import Role from './entity/Role'
import Authority from './entity/Authority'

const authorityTreeURL = '/role/authority/tree' // 权限指令树
const authorityListURL = '/role/authority/list' // 权限指令列表
const saveURL = '/role' // 新增
const updateURL = '/role/{dv}' // 修改
const markDeleteByIdURL = '/role/{dv}' // 按 dv  逻辑删除
const markDeleteByIdsURL = '/role' // 按 dv  批量逻辑删除
const pageURL = '/role/page/{number}/{limit}' // 分页：多条件批量查询
const optionsURL = '/role/options'// 角色下拉列表选项

/**
 * 后台服务请求：角色
 * @author 谢长春 2019-7-28
 */
export default class RoleService {
  /**
   * 查询权限指令树
   * @return {Promise<Result>}
   */
  static async getAuthorityTree() {
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(authorityTreeURL)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new Authority(row))
  }

  /**
   * 查询权限指令列表
   * @return {Promise<Result>}
   */
  static async getAuthorityList() {
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(authorityListURL)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new Authority(row))
  }

  /**
   * 新增
   * @param vo {Role}
   * @return {Promise<Result>}
   */
  static async save(vo = new Role()) {
    const {name, remark, authorityTree} = vo.clearBlankValue()
    return await http
      .post(saveURL, {name, remark, authorityTree})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 修改
   * @param vo {Role}
   * @return {Promise<Result>}
   */
  static async update(vo = new Role()) {
    const {id, dv, ...body} = vo
    return await http
      .put(updateURL.format(dv || 0), body)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv  逻辑删除
   * @param dv {string} {@link Role#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteById(dv) {
    return await http
      .patch(markDeleteByIdURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv  批量逻辑删除
   * @param dvs {string[]} {@link Role#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteByIds(dvs) {
    return await http
      .patch(markDeleteByIdsURL, dvs)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 分页：多条件批量查询
   * @param vo {Role}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new Role()) {
    const {page, ...params} = vo
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new Role(row))
  }

  /**
   * 角色下拉列表选项
   * @return {Promise<Result>}
   */
  static async options() {
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(optionsURL)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new Role(row))
  }
}
