import lodash from 'lodash'
import http from './http'
import Result from '../utils/entity/Result'
import Page from '../utils/entity/Page'
import User from './entity/User'
import Rsa from '@/utils/Rsa'

const loginURL = '/open/auth/login/OM_PC' // 登录
const logoutURL = '/logout' // 退出
const currentUserURL = '/user/current' // 当前登录用户信息
const updateNicknameURL = '/user/nickname' // 修改昵称
const saveURL = '/user' // 新增用户
const updateURL = '/user/{dv}' // 修改用户信息
const markDeleteByIdURL = '/user/{dv}' // 按 dv  逻辑删除
const markDeleteByIdsURL = '/user' // 按 dv  批量逻辑删除
const pageURL = '/user/page/{number}/{limit}' // 分页查看用户信息
const resetPasswordURL = '/user/reset-password/{dv}' // 重置密码
const changePasswordURL = '/user/change-password' // 当前登录用户修改密码
const changeAvatarURL = '/user/change-avatar' // 当前登录用户修改头像
const enableURL = '/user/enable/{dv}' // 禁用账号
const disableURL = '/user/disable/{dv}' // 启用账号

// users: '/user/users/{roleId}' // 按角色查询用户集合
let token = ''
/**
 *
 * @param xtoken {string}
 */
const setTokenCache = (xtoken) => {
  token = xtoken
  if (process.env.GULPFILE) {
    // gulp 运行环境不支持 localStorage
  } else {
    localStorage.setItem('x-token', token)
  }
}
/**
 *
 * @return {string}
 */
const getTokenCache = () => {
  if (!token) {
    if (process.env.GULPFILE) {
      // gulp 运行环境不支持 localStorage
    } else {
      return localStorage.getItem('x-token')
    }
  }
  return token
}
const clearTokenCache = () => {
  token = ''
  if (process.env.GULPFILE) {
    // gulp 运行环境不支持 localStorage
  } else {
    localStorage.removeItem('x-token')
  }
}

/**
 * 后台服务请求：用户
 * @author 谢长春 2019-7-28
 */
export default class UserService {
  /**
   * 获取登录接口返回的 token
   * @return {string}
   */
  static getToken() {
    return getTokenCache()
  }

  /**
   * 获取登录接口返回的 token
   * @return {object} {'x-token':'header token'}
   */
  static getTokenHeader() {
    return {'x-token': getTokenCache()}
  }

  /**
   * 检查 token 是否存在
   * @return {boolean} true：存在，false：不存在
   */
  static tokenExists() {
    return !lodash.isEmpty(getTokenCache())
  }

  /**
   * 清除 token
   */
  static clearToken() {
    clearTokenCache()
  }

  /**
   * 登录
   * @param username {string} {@link User#username}
   * @param password {string} {@link User#password}
   * @return {Promise<Result>}
   */
  static async login(username, password) {
    return await http
      .post(loginURL, {
        username,
        password: Rsa.encrypt(password)
      })
      .then((res) => {
        const result = Result.ofResponse(res)
        result.dataFirst(token => {
          console.log('x-token: ' + token)
          setTokenCache(token)
        })
        return result
      })
      .catch(Result.ofCatch)
  }

  /**
   * 退出
   * @return {Promise<Result>}
   */
  static async logout() {
    const res = await http.post(logoutURL)
    if (res.status === 200) {
      clearTokenCache()
      return new Result(0, 'A00000', '成功')
    }
    return new Result(0, 'A00001', '失败')
  }

  /**
   * 获取当前登录用户信息
   * @return {Promise<Result>}
   */
  static async getCurrentUser() {
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(currentUserURL)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new User(row))
  }

  /**
   * 修改昵称
   * @param nickname {string} {@link User#nickname}
   * @return {Promise<Result>}
   */
  static async updateNickname(nickname) {
    return await http
      .patch(updateNicknameURL, {nickname})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 新增
   * @param vo {User}
   * @return {Promise<Result>}
   */
  static async save(vo = new User()) {
    const {password, ...params} = vo.clearBlankValue()
    return await http
      .post(saveURL, {...params, password: Rsa.encrypt(password)})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 修改
   * @param vo {User}
   * @return {Promise<Result>}
   */
  static async update(vo = new User()) {
    const {id, dv, ...body} = vo
    return await http
      .put(updateURL.format(dv || 0), body)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv  逻辑删除
   * @param dv {string} {@link User#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteById(dv) {
    return await http
      .patch(markDeleteByIdURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 按 dv  批量逻辑删除
   * @param dvs {string[]} {@link User#dv}
   * @return {Promise<Result>}
   */
  static async markDeleteByIds(dvs) {
    return await http
      .patch(markDeleteByIdsURL, dvs)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 分页：多条件批量查询
   * @param vo {User}
   * @return {Promise<Result>}
   */
  static async pageable(vo = new User()) {
    const {page, avatarUrl, ...params} = vo
    /**
     *
     * @type {Result<any>}
     */
    const result = await http
      .get(pageURL.formatObject(page || Page.ofDefault()), {params})
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
    return result.dataTransform(row => new User(row))
  }

  /**
   * 重置用户密码，返回新密码
   * @param dv {string} {@link User#dv}
   * @return {Promise<Result>}
   */
  static async resetPassword(dv) {
    return await http
      .patch(resetPasswordURL.format(dv || 0))
      .then(Result.ofResponse)
      // .then(res=>{
      //   const result = Result.ofResponse(res)
      //   result.dataFirst(row=>{
      //     result.data = [Rsa.decrypt(row)]
      //   })
      //   return result
      // })
      .catch(Result.ofCatch)
  }

  /**
   * 当前登录用户修改密码
   * @param passwordOld {string} 原密码
   * @param passwordNew  {string} 新密码
   * @return {Promise<Result>}
   */
  static async changePassword(passwordOld, passwordNew) {
    return await http
      .patch(changePasswordURL, {
        passwordOld: Rsa.encrypt(passwordOld),
        passwordNew: Rsa.encrypt(passwordNew)
      })
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 当前登录用户修改头像
   * @param avatar {FileDTO} 新头像
   * @return {Promise<Result>}
   */
  static async changeAvatar(avatar) {
    return await http
      .patch(changeAvatarURL, avatar)
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 启用账号
   * @param dv {string} {@link User#dv}
   * @return {Promise<Result>}
   */
  static async enable(dv) {
    return await http
      .patch(enableURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  /**
   * 禁用账号
   * @param dv {string} {@link User#dv}
   * @return {Promise<Result>}
   */
  static async disable(dv) {
    return await http
      .patch(disableURL.format(dv || 0))
      .then(Result.ofResponse)
      .catch(Result.ofCatch)
  }

  // /**
  //  * 按角色查询用户集合
  //  * @param roleId {User#roleId}
  //  * @return {Promise<Result>}
  //  */
  // static async getUsersFromRole(roleId) {
  //   return await http
  //     .get(usersURL.format(roleId))
  //     .then(Result.ofResponse)
  //     .catch(Result.ofCatch);
  // }
}
