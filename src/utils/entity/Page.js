/**
 * 分页对象
 * @author 谢长春 2019-7-28
 */
export default class Page {
  /**
   * 使用默认值初始化分页对象
   * @return {Page}
   */
  static ofDefault() {
    return new Page()
  }

  /**
   * 使用默认值初始化分页对象
   * @param limit {number}
   * @return {Page}
   */
  static limit(limit) {
    return new Page(1, limit)
  }

  /**
   * 构造分页对象
   * @param number {number} 当前页码，从 1 开始
   * @param limit {number} 每页返回数据行数，默认值为 10
   * @param pageCount {number} 总页数
   * @param totalCount {number} 总数据行数
   * @return {Page}
   */
  static of({
              number,
              limit,
              pageCount = 0,
              totalCount = 0
            } = {}) {
    return new Page(number, limit, pageCount, totalCount)
  }

  /**
   * 构造分页对象
   * @param number {number} 当前页码，从 1 开始
   * @param limit {number} 每页返回数据行数，默认值为 10
   * @param pageCount {number} 总页数
   * @param totalCount {number} 总数据行数
   */
  constructor(number = 1, limit = 10, pageCount = 0, totalCount = 0) {
    /**
     * 当前页码，从 1 开始
     * @type {number}
     */
    this.number = number
    /**
     * 每页返回数据行数
     * @type {number}
     */
    this.limit = limit
    /**
     * 总页数
     * @type {number}
     */
    this.pageCount = pageCount
    /**
     * 总数据行数
     * @type {number}
     */
    this.totalCount = totalCount
  }

  toString() {
    return JSON.stringify(this)
  }
}
